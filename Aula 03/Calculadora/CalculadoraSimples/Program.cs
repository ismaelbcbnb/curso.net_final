﻿using CalculadoraSimples;
using System.Runtime.InteropServices;

Calculadora conta = new Calculadora(15,25);

double soma = conta.Somar();
double diferenca = conta.Subtrair();
double produto = conta.Multiplicar();
double quociente = conta.Dividir();

Console.WriteLine(conta.Numero1 + " + " + conta.Numero2 + " = " + soma.ToString());
Console.WriteLine(conta.Numero1 + " - " + conta.Numero2 + " = " + diferenca.ToString());
Console.WriteLine(conta.Numero1 + " x " + conta.Numero2 + " = " + produto.ToString());
Console.WriteLine(conta.Numero1 + " / " + conta.Numero2 + " = " + quociente.ToString());



