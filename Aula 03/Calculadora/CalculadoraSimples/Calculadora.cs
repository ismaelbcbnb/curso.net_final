﻿namespace CalculadoraSimples
{
    internal class Calculadora
    {
        public double Numero1 { get; private set; }
        public double Numero2 { get; private set; }

        public Calculadora(double num1, double num2) {
            this.Numero1 = num1;
            this.Numero2 = num2;
        }

        public double Somar()
        {
            return Numero1 + Numero2;
        }

        public double Subtrair()
        {
            return Numero1 - Numero2;
        }

        public double Multiplicar()
        {
            return Numero1 * Numero2;
        }

        public double Dividir()
        {
            return Numero1 / Numero2;
        }

    }
}
